# makefmk

Outil permettant d'automatiser la création et la mise à jour de paquets
pour la Framakey sous Linux.

## Dépendances

 - firejail
 - upx
 - wine

## Exemple : création automatique de Notepad++Portable

Les fichiers utilisés pour créer le paquet sont disponibles dans le
dossier `apps/Notepad++Portable`.

Créer manuellement la structure du paquet :

    Notepad++Portable
    ├── App
    │   └── AppInfo
    │       ├── appicon_16.png
    │       ├── appicon_24.png
    │       ├── appicon.ico
    │       ├── appinfo.ini
    │       └── ...
    ├── Data
    └── Other
        └── Source
            ├── Notepad++Portable.ahk
            └── ...

Dans le dossier Other/Source, créer le script bash `fmkbuild.sh` :

    # Version de l'application (obligatoire)
    pkgver="6.7.9.0"
    # Numéro de release du paquet (obligatoire)
    pkgrel="01"

    # Variables utilisées pour télécharger notepad++
    srcver=$(echo $pkgver | sed 's/\(\.0\)*$//')
    srcmajorver=$(echo $pkgver | sed -r 's/^([0-9]+)\..*$/\1.x/')

    # Tableau de fichiers à télécharger (obligatoire)
    sources=("https://notepad-plus-plus.org/repository/$srcmajorver/$srcver/npp.$srcver.bin.zip")

    # Fonction se chargeant d'installer l'application dans le dossier $builddir (obligatoire)
    function build {
        unzip -q "$builddir/npp.$srcver.bin.zip" -d "$builddir/Notepad++"

        cp $builddir/Notepad++/localization/french.xml $builddir/Notepad++/nativeLang.xml

        for path in $builddir/Notepad++/localization/*.xml; do
            filename=$(basename $path)
            if [ "$filename" != "french.xml" ] && [ "$filename" != "english.xml" ]; then
                rm $path
            fi
        done
    }

    # Fonction se chargeant de créer le paquet dans $pkgdir (obligatoire)
    function package {
        mv "$builddir/Notepad++" "$pkgdir/App"
    }

Finalement, lancer la commande `makefmk <chemin vers le dossier Notepad++Portable>`.
Le paquet sera alors créé dans une "sandbox" wine, puis déplacé dans le dossier courant.

Pour créer le paquet, makefmk :

 - Charge le fichier `fmkbuild.sh`
 - Crée une "sandbox" wine
 - Si les fichiers du tableau `sources` ne sont pas présents dans le dossier
   courant, les télécharge
 - Crée un dossier `$builddir` dans la sandbox, et y copie les fichiers
   téléchargés
 - Lance la fonction `build`, qui installe l'application dans le dossier
   `$builddir`
 - Crée un dossier `$pkgdir` dans la sandbox, et y copie la structure du
   paquet
 - Lance la fonction `package` qui déplace les fichiers du dossier `$builddir`
   vers le dossier `$pkgdir/App/<AppName>`
 - Compile le(s) lanceur(s) (tous les fichiers `Other/Source/*Portable.ahk`)
 - Met à jour le fichier appinfo.ini
 - Crée le paquet et le copie dans le dossier courant.
