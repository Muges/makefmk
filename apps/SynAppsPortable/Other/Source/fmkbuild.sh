pkgver="0.2.0.7"
pkgrel="01"

srcver=$(echo $pkgver | sed 's/\(\.0\)*$//')
sources=("archive.zip::https://git.framasoft.org/Muges/SynApps/repository/archive.zip?ref=v$srcver")

function build {
    mkdir "$builddir/Apps"
    mkdir -p "$builddir/Framakey/SynAppsPortable/App"

    unzip -q "$builddir/archive.zip" -d "$builddir/Framakey/SynAppsPortable/App"

    # Crée la base de donnée et télécharge les icones
    cd "$builddir/Framakey/SynAppsPortable/App/SynApps.git"
    env python2 ./main.py update silent

    rm "$builddir/Framakey/SynAppsPortable/App/SynApps.git/.gitignore"
    rm "$builddir/Framakey/SynAppsPortable/App/SynApps.git/debug.log"
}

function package {
    mv "$builddir/Framakey/SynAppsPortable/App/SynApps.git" "$pkgdir/App/SynApps"
}
