﻿;===============================================================================
; Function Name:	CalcRelPath
; Description:		Calcule les chemins relatifs entre les deux dossiers
; Parameters:		dossier référence (chemin)
;					dossier à référencer (chemin)
;					dossier de base à utiliser pour les chemins relatifs donnés
;===============================================================================
CalcRelPath(_OrigDir, _RelDir, _BaseDir="")
{
	; On commence par convertir les éventuels chemins relatifs en absolus
	_OrigDir := ConvRel2Abs(_OrigDir, _BaseDir)
	_RelDir := ConvRel2Abs(_RelDir, _BaseDir)
	; 1e cas simple _RelDir est un sous répertoire de _OrigDir
	IfInString, _RelDir, %_OrigDir%
		StringReplace, _OutputPath, _RelDir, %_OrigDir%, .
	Else
		{
		; on vérifie que les lecteurs soient identiques
		SplitPath, _OrigDir, , , , , _OrigDrive
		SplitPath, _RelDir, , , , , _RelDrive
		If (_OrigDrive != _RelDrive)
			{
			MsgBox, 16, Erreur, %_OrigDir% et %_RelDir% sont sur des lecteurs différents !, 3
			ExitApp
			}
		_ConcatPath := ""
		; recherche de la partie commune du chemin
		Loop, parse, _OrigDir, \
			{
			IfInString, _RelDir, %_ConcatPath%%A_LoopField%
				_ConcatPath .= A_LoopField . "\"
			Else
				Break
			}
		; effacement de la partie commune du chemin
		StringReplace, _OutputPath, _RelDir, %_ConcatPath%
		StringReplace, _OrigDir, _OrigDir, %_ConcatPath%
		; comptage du nombre de niveaux restants dans le chemin référence
		StringReplace, _Poubelle, _OrigDir, \, |, UseErrorLevel
		_Count := ErrorLevel + 1
		Loop, %_Count%
			{
			_OutputPath := "..\" . _OutputPath
			}
		}
Return %_OutputPath%
}

;===============================================================================
; Function Name:	ConvRel2Abs
; Description:		Convertit un chemin relatif en absolu
; Parameters:		_relpath : chemin relatif
;					_basepath : chemin de base, A_ScriptDir si aucun
;===============================================================================
ConvRel2Abs(_relpath, _basepath ="")
{
	_basepath := (_basepath) ? _basepath : A_ScriptDir
	If _relpath contains ..\,.\
		{
		StringReplace, _relpath, _relpath, ..\, , UseErrorLevel
		If ErrorLevel
			{
			_Count := ErrorLevel
			StringLeft, _Tampon, _basepath, InStr(_basepath, "\", 0, 0, _Count)
			_relpath := _Tampon . _relpath
			}
		Else
			StringReplace, _relpath, _relpath, .\, %_basepath%\
		StringReplace, _relpath, _relpath, .\, \, All
		StringReplace, _relpath, _relpath, \\, \, All
		}
Return _relpath
}