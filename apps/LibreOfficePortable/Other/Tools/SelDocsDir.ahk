﻿
; --------------------------------------------------------------------------------------------------
; 									SelDocsDir 0.1.0.0
; --------------------------------------------------------------------------------------------------
; Par fat115
; License : GPL
; Ce script permet de créer SelDocsDir.

; Fonctions du script:
;	Sélectionne le dossier des documents pour LibreOffice
; Compiler: AutoHotkey_L (1.0.92.2+) (http://www.autohotkey.net/~Lexikos/AutoHotkey_L/).

; $id=SelDocsDir.ahk $date=2011-05-22
; --------------------------------------------------------------------------------------------------

;Copyright (C) 2005-2011 Framakey

;Website: http://www.framakey.org

;This software is OSI Certified Open Source Software.
;OSI Certified is a certification mark of the Open Source Initiative.

;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either version 2
;of the License, or (at your option) any later version.

;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
;GNU General Public License for more details.

;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA	02110-1301, USA.
; ----------------------------------------------------
#NoEnv
#SingleInstance

#Include %A_ScriptDir%\AhkIncludes
#Include CalcRelPath.ahk

SetWorkingDir, %A_ScriptDir%
StringCaseSense, Locale

StringReplace, p_AppDir, A_ScriptDir, \Other\Tools
SplitPath, p_AppDir, , , , , s_Drive
p_SettingsDir := "..\..\Data\settings"
p_RegModXCU := "\user\registrymodifications.xcu"
s_PathCurrentWork = <item oor:path="/org.openoffice.Office.Common/Path/Current"><prop oor:name="Work" oor:op="fuse"><value xsi:nil="true"/></prop></item>
s_PathInfoWChanged = <item oor:path="/org.openoffice.Office.Common/Path/Info"><prop oor:name="WorkPathChanged" oor:op="fuse"><value>true</value></prop></item>

IfNotExist, %p_SettingsDir%%p_RegModXCU%
	FileSelectFolder, p_SettingsDir, *%p_AppDir%, 0, Choisissez le dossier du profil (\Data\settings par défaut) :

; Vérification des paramètres
Gosub VerifParam

FileRead, s_RegMod, %p_SettingsDir%%p_RegModXCU%
If !s_RegMod
	ExitApp

If (s_Action = "")
	{
	MsgBox, 36, Sélection du dossier des documents, Voulez-vous utiliser le dossier Mes documents ?
	IfMsgBox, Yes
		s_Action := "-d"
	Else
		{
		MsgBox, 36, Sélection du dossier des documents, Voulez-vous utiliser un dossier de la clé (relatif) ?
		IfMsgBox, Yes
			{
			s_Action := "-r"
			FileSelectFolder, p_Chemin, %s_Drive%\ *%p_AppDir%, 0, Choisissez le dossier à utiliser
			}
		Else
			{
			s_Action := "-a"
			FileSelectFolder, p_Chemin, ::{20d04fe0-3aea-1069-a2d8-08002b30309d}, 0, Choisissez le dossier à utiliser.`nLes modifications ne seront pas gérées par le lanceur.
			}
		}
	}

If (s_Action = "-d")
	{
	s_RegMod := DelCustDir(s_RegMod)
	}
Else If (s_Action = "-r")
	{
	s_RegMod := DelCustDir(s_RegMod)
	p_Chemin := ConvertRelPath(p_Chemin)
	s_RegMod := InsCustDir(s_RegMod, p_Chemin)
	}
Else If (s_Action = "-a")
	{
	s_RegMod := DelCustDir(s_RegMod)
	p_Chemin := ConvertAbsPath(p_Chemin)
	s_RegMod := InsCustDir(s_RegMod, p_Chemin)
	}

FileDelete, %p_SettingsDir%%p_RegModXCU%
FileAppend, %s_RegMod%, %p_SettingsDir%%p_RegModXCU%

ExitApp
Return

VerifParam:
{
	If 0 = 0
		Return
	s_Action = %1%
	If s_Action not in -d,-r,-a
		ExitApp
	; le reste des paramètres est considéré comme un chemin
	p_Chemin := ""
	Loop, %0%
		{
		If A_Index = 1
			Continue
		_param := %A_Index%
		p_Chemin .= (p_Chemin) ? " " . _param : _param
		}
	StringReplace, p_Chemin, p_Chemin, ", , A
	; "
Return
}

ConvertAbsPath(_path)
{
	StringReplace, _path, _path , %A_Space%, `%20, A
	StringReplace, _path, _path , \, /, A
	_path := "file:///" . _path
	Return _path
}

ConvertRelPath(_path)
{
	StringReplace, _basepath, A_ScriptDir, \Other\Tools, \App\LibreOffice\Basis\program
	_path := CalcRelPath(_basepath, _path)
	StringReplace, _path, _path , %A_Space%, `%20, A
	StringReplace, _path, _path , \, /, A
	_path := "$(prog)/" . _path
	Return _path
}

DelCustDir(_string)
{
	Global s_PathCurrentWork
	Global s_PathInfoWChanged
	StringReplace, _string, _string, %s_PathCurrentWork%
	StringReplace, _string, _string, %s_PathInfoWChanged%
	_Needle = <item\Q oor:path="/org.openoffice.Office.Paths/Paths/org.openoffice.Office.Paths:NamedPath['Work']"\E>.*?</item>
	_string := RegExReplace(_string, _Needle)
	Return _string
}

InsCustDir(_string, _dir)
{
	Global s_PathCurrentWork
	Global s_PathInfoWChanged
	_part1 = <item oor:path="/org.openoffice.Office.Paths/Paths/org.openoffice.Office.Paths:NamedPath['Work']"><prop oor:name="WritePath" oor:op="fuse"><value>
	_part2 = </value></prop></item>
	_newlocation := s_PathCurrentWork . s_PathInfoWChanged . _part1 . _dir . _part2 . "</oor:items>"
	StringReplace, _string, _string, </oor:items>, %_newlocation%
	Return _string
}
