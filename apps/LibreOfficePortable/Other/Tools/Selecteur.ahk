﻿AppName := "LibreOfficePortable"

Gui, Add, GroupBox, x6 y2 w320 h150 , Sélectionnez le lanceur désiré :
Gui, Add, Radio, x26 y22 w290 h30 vRadioGroup, Lanceur pour clés USB
Gui, Add, Radio, x26 y62 w290 h30 , Lanceur pour média en lecture seule
Gui, Add, Radio, x26 y102 w290 h30 , Lanceur pour utilisation réseau
Gui, Add, Button, x6 y162 w160 h30 gAppliquer, Appliquer
Gui, Add, Button, x176 y162 w150 h30 gAnnuler, Annuler
; Generated using SmartGUI Creator 4.0
Gui, Show, x131 y91 h204 w335, Sélecteur
Return

GuiClose:
ExitApp

Annuler:
ExitApp

Appliquer:
Gui, Submit, NoHide
If (RadioGroup = 1)
	{
	FileCopy, %AppName%.exe, ..\..\%AppName%.exe, 1
	StringReplace, _path, A_ScriptDir, Apps\LibreOfficePortable\Other\Tools, Data\Documents
	RunWait, SelDocsDir.exe -r "%_path%"
	}
Else If (RadioGroup = 2)
	{
	FileCopy, %AppName%_RO.exe, ..\..\%AppName%.exe, 1
	RunWait, SelDocsDir.exe -d
	}
Else If (RadioGroup = 3)
	{
	FileCopy, %AppName%_N.exe, ..\..\%AppName%.exe, 1
	RunWait, SelDocsDir.exe -d
	}
ExitApp
