; ----------------------------------------------------
; LibreOfficePortable Individual App
; ----------------------------------------------------
; Par fat115
; License : GPL
; Ce script permet de cr�er les lanceurs individuels de LibreOfficePortable.
; Compiler: NSIS (http://www.nullsoft.com).
; $id=LibreOfficePortable.nsi $date=2011-10-23
; ----------------------------------------------------
;Copyright (C) 2005-2011 Framakey

;Website: http://www.framakey.org

;This software is OSI Certified Open Source Software.
;OSI Certified is a certification mark of the Open Source Initiative.

;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either version 2
;of the License, or (at your option) any later version.

;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
;GNU General Public License for more details.

;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA	02110-1301, USA.
; ----------------------------------------------------
; G�n�ral
; ---- Modifiez les valeurs souhait�es ----
; D�finition variables ent�te
!searchparse /file "..\..\..\App\AppInfo\appinfo.ini" `AppID=` BASENAME
!searchreplace APPNAME ${BASENAME} "Portable" ""

!ifdef BASE
	!define COMPONENT "Base"
!else ifdef CALC
	!define COMPONENT "Calc"
!else ifdef DRAW
	!define COMPONENT "Draw"
!else ifdef IMPRESS
	!define COMPONENT "Impress"
!else ifdef MATH
	!define COMPONENT "Math"
!else ifdef WRITER
	!define COMPONENT "Writer"
!endif

!define FULLNAME "${APPNAME}${COMPONENT}Portable"
!define COMPANY "Framakey.org" ;Nom du groupe d�veloppant le projet


OutFile "..\..\..\${FULLNAME}.exe"
; Nom de l'ex�cutable
Name "${FULLNAME}"

; Icone
Icon "..\..\..\App\AppInfo\${FULLNAME}.ico"
WindowIcon Off

; Options de Compilation
CRCCheck On
SilentInstall Silent
AutoCloseWindow True
RequestExecutionLevel user

; Compression Optimale
SetCompress Auto
SetCompressor /SOLID lzma
SetCompressorDictSize 32
SetDatablockOptimize On

; ----------------------------------------------------
; Liste des includes
;(Standard NSIS)
!include FileFunc.nsh
!include LogicLib.nsh
!include StrFunc.nsh
${StrCase}

; ----------------------------------------------------
; Informations de Version
VIProductVersion "1.0.0.0"
VIAddVersionKey ProductName "${FULLNAME}"
VIAddVersionKey FileDescription "${FULLNAME} pour FramaKey"
VIAddVersionKey LegalTrademarks "${APPNAME} par Framakey"
VIAddVersionKey LegalCopyright "� 2005-2011 Framakey - GNU/GPL v2"
VIAddVersionKey CompanyName "Framakey"
VIAddVersionKey FileVersion "1.0.0.0"

; ----------------------------------------------------
; Variables

Var Parameters
Var CompApp

Section "Main"
; R�cup�ration des param�tres pass�s au lanceur (par exemple par CAFE)
	${GetParameters} $Parameters
	${StrCase} $CompApp ${COMPONENT} "L"
	
	SetOutPath $EXEDIR
	Exec '${BASENAME}.exe -$CompApp $Parameters'

SectionEnd