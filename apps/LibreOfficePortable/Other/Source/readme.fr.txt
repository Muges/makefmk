LibreOfficePortable Lanceur v1.0.0.1
======================================
Ecrit par fat115 d'apr�s une id�e originale de John T. Haller

Website: http://www.framakey.org/


Changelog LibreOfficePortable
=========================
R��criture compl�te du lanceur pour LibreOffice 3.2.1


APropos LibreOfficePortable
=========================
Le lanceur LibreOfficePortable vous permet d'utiliser LibreOffice � partir d'un support amovible dont la lettre change d'un ordinateur � un autre.
Les extensions suppl�mentaires suivantes sont incluses dans la distribution :
- MySQL Connector : http://extensions.services.openoffice.org/fr/project/mysql_connector (GNU LGPL)

Trois types de lancement possibles : � partir d'une cl� USB, � partir d'un m�dia en lecture seule, � partir d'un emplacement r�seau avec sauvegarde des pr�f�rences dans un dossier personnel.
Pour passer d'un type � l'autre, il suffit de lancer le petit utilitaire \Other\Tools\Selecteur.exe. Celui-ci se chargera de copier le bon lanceur � la racine de l'application.
Vous pouvez le faire manuellement en copier un des trois lanceurs pr�sents dans le dossier et en le renommant LibreOfficePortable.exe
Les cas sont les suivants :
- LibreOfficePortable.exe => c'est le lanceur classique pour cl� USB.
- LibreOfficePortable_N.exe => c'est le lanceur sp�cifique pour lecteur r�seau (Network)avec sauvegarde des pr�f�rences.
- LibreOfficePortable_RO.exe => c'est le lanceur sp�cifique pour m�dia en lecture-seule. Les pr�f�rences ne sont pas enregistr�es.
En pratique les deux derniers lanceurs n'�crivent pas dans le dossier LibreOfficePortable ou un de ses sous-dossiers ... sauf si un petit malin essaye d'installer une extension de mani�re globale :D
Le s�lecteur modifiera �galement le profil de base pour faire pointer le dossier des documents ou vers le dossier \Data\Documents de la Framakey (cas du lanceur classique) ou vers "Mes Documents" de l'utilisateur en cours (cas des deux autres lanceurs)
Il est possible de changer le dossier ainsi d�fini � l'aide du petit utilitaire \Other\Tools\SelDocsDir.exe.
vous pourrez choisir d'utiliser le dossier "Mes documents", un dossier autre sur la cl� vers lequel pointera toujours LibreOfficePortable ou un dossier pour lequel le chemin absolu sera inscrit dans le profil (peut-�tre utile en avec le lanceur r�seau).

Des lanceurs pours les modules individuels (Base, Calc, Draw, Impress, Math et Writer) sont �galement inclus. Ils s'appuient tous sur la version de LibreOfficePortable.exe pr�sente dans le dossier racine (LibreOfficePortable).

Fichiers source
===============
LibreOfficePortable.ahk
LibreOfficeComponentPortable.nsi (dans le dossier \Other\Source\Components)
LibreOfficePortable.ini
Selecteur.ahk (dans le dossier \Other\Tools\)
SelDocsDir.ahk (dans le dossier \Other\Tools\)

LICENCE
=======
Voir Licence.txt


CONFIGURATION LibreOfficePortable.ini 
=================================
Le lanceur LibreOfficePortable recherchera un �ventuel fichier LibreOfficePortable.ini dans son r�pertoire.
Si les options par d�faut vous conviennent, il n'y a pas lieu de l'utiliser.
Le format du fichier est le suivant :

[LibreOfficePortable]
ApplicationDirectory=App\LibreOffice\program
ApplicationExecutable=soffice.exe
SettingsDirectory=Data\settings
NetworkSettingsDirectory=P:\Profile\LibreOffice
AdditionalParameters=

ApplicationDirectory : nom du sous-dossier contenant les fichiers de l'application, relatif � l'emplacement du lanceur.
ApplicationExecutable : nom de l'ex�cutable � utiliser par le lanceur.
SettingsDirectory : nom du sous-dossier contenant le profil personnel, relatif � l'emplacement du lanceur.
NetworkSettingsDirectory : Chemin absolu vers le profil r�seau de l'utilisateur.
AdditionalParameters : les param�tres additionnels � passer � l'application.

Toutes ces valeurs sont facultatives. Les valeurs indiqu�es dans l'exemple sont celles qui sont cod�es "en dur" dans les lanceurs.

Toute valeur non vide sera prise en compte.
Par exemple, Si vous voulez d�sactiver le splashscreen, il suffit de copier LibreOfficePortable.ini � c�t� de LibreOfficePortable.exe et de modifier la ligne suivante :
AdditionalParameters=-nologo


Compilation des lanceurs 
========================
Les lanceurs principaux (LibreOfficePortable.exe et ses d�riv�s _N et _RO) doivent �tre compil�s avec AutoHotkey_L v1.1+
Les lanceurs individuels ne sont que des wrappers. si vous voulez les recompiler (par exemple pour changer les ic�nes), vous devez disposer de NSIS 2.46 et passer une des optiosn suivantes au compilateur :
makensis.exe /DBASE LibreOfficeComponentPortable.nsi => pour cr�er le lanceur spcifique au composant Base
makensis.exe /DCALC LibreOfficeComponentPortable.nsi => pour cr�er le lanceur spcifique au composant Calc
makensis.exe /DDRAW LibreOfficeComponentPortable.nsi => pour cr�er le lanceur spcifique au composant Draw
makensis.exe /DIMPRESS LibreOfficeComponentPortable.nsi => pour cr�er le lanceur spcifique au composant Impress
makensis.exe /DMATH LibreOfficeComponentPortable.nsi => pour cr�er le lanceur spcifique au composant Math
makensis.exe /DWRITER LibreOfficeComponentPortable.nsi => pour cr�er le lanceur spcifique au composant Writer
Un conseil : utilisez le dernier pack de portabilisation disponible sur cette page => http://framakey.org/Portables/PackDePortabilisationFramakey

