# Version de l'application (obligatoire)
pkgver="4.4.3.0"
# Numéro de release du paquet (obligatoire)
pkgrel="01"

srcver=$(echo $pkgver | sed 's/\(\.0\)*$//')

# Tableau de fichiers à télécharger (obligatoire)
sources=("http://download.documentfoundation.org/libreoffice/stable/$srcver/win/x86/LibreOffice_${srcver}_Win_x86.msi")

# Fonction se chargeant d'installer l'application dans le dossier $builddir (obligatoire)
function build {
    wine msiexec /qb /i $builddir/LibreOffice_${srcver}_Win_x86.msi \
    INSTALLLOCATION=$builddir/LibreOffice REGISTER_NO_MSO_TYPES=1 \
    UI_LANGS=fr_FR ISCHECKFORPRODUCTUPDATES=0 REBOOTYESNO=No QUICKSTART=0 \
    ADDLOCAL=ALL VC_REDIST=0 REMOVE=gm_o_Onlineupdate,gm_r_ex_Dictionary_Af, \
    gm_r_ex_Dictionary_An,gm_r_ex_Dictionary_Ar,gm_r_ex_Dictionary_Be, \
    gm_r_ex_Dictionary_Bg, gm_r_ex_Dictionary_Bn,gm_r_ex_Dictionary_Br, \
    gm_r_ex_Dictionary_Pt_Br,gm_r_ex_Dictionary_Pt_Pt,gm_r_ex_Dictionary_Ca, \
    gm_r_ex_Dictionary_Cs,gm_r_ex_Dictionary_Da,gm_r_ex_Dictionary_Nl, \
    gm_r_ex_Dictionary_Et,gm_r_ex_Dictionary_Gd,gm_r_ex_Dictionary_Gl, \
    gm_r_ex_Dictionary_Gu,gm_r_ex_Dictionary_De,gm_r_ex_Dictionary_He, \
    gm_r_ex_Dictionary_Hi,gm_r_ex_Dictionary_Hu,gm_r_ex_Dictionary_It, \
    gm_r_ex_Dictionary_Lt,gm_r_ex_Dictionary_Lv,gm_r_ex_Dictionary_Ne, \
    gm_r_ex_Dictionary_No,gm_r_ex_Dictionary_Oc,gm_r_ex_Dictionary_Pl, \
    gm_r_ex_Dictionary_Ro,gm_r_ex_Dictionary_Ru,gm_r_ex_Dictionary_Si, \
    gm_r_ex_Dictionary_Sk,gm_r_ex_Dictionary_Sl,gm_r_ex_Dictionary_El, \
    gm_r_ex_Dictionary_Es,gm_r_ex_Dictionary_Sv,gm_r_ex_Dictionary_Te, \
    gm_r_ex_Dictionary_Th,gm_r_ex_Dictionary_Vi,gm_r_ex_Dictionary_Zu
    
    cp "$(winepath -u 'C:\\windows\system32\msvcr110.dll')" "$builddir/LibreOffice/program"
    cp "$(winepath -u 'C:\\windows\system32\msvcp110.dll')" "$builddir/LibreOffice/program"
}

# Fonction se chargeant de créer le paquet dans $pkgdir (obligatoire)
function package {
    mv "$builddir/LibreOffice" "$pkgdir/App"
}
