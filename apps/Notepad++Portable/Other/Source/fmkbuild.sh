# Version de l'application (obligatoire)
pkgver="6.7.9.1"
# Numéro de release du paquet (obligatoire)
pkgrel="01"

# Variables utilisées pour télécharger notepad++
srcver=$(echo $pkgver | sed 's/\(\.0\)*$//')
srcmajorver=$(echo $pkgver | sed -r 's/^([0-9]+)\..*$/\1.x/')

# Tableau de fichiers à télécharger (obligatoire)
sources=("https://notepad-plus-plus.org/repository/$srcmajorver/$srcver/npp.$srcver.bin.zip")

# Fonction se chargeant d'installer l'application dans le dossier $builddir (obligatoire)
function build {
    # Décompresse l'archive
    unzip -q "$builddir/npp.$srcver.bin.zip" -d "$builddir/Notepad++"
    
    # Utilise la traduction française par défaut
    cp $builddir/Notepad++/localization/french.xml $builddir/Notepad++/nativeLang.xml
    
    # Supprime les fichiers de traduction
    for path in $builddir/Notepad++/localization/*.xml; do
        filename=$(basename $path)
        if [ "$filename" != "french.xml" ] && [ "$filename" != "english.xml" ]; then
            rm $path
        fi
    done
}

# Fonction se chargeant de créer le paquet dans $pkgdir (obligatoire)
function package {
    mv "$builddir/Notepad++" "$pkgdir/App"
}
