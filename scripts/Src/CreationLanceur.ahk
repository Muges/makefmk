﻿/*
 * * * Compile_AHK SETTINGS BEGIN * * *


[AHK2EXE]
Exe_File=%In_Dir%\..\CreationLanceur.exe
Execution_Level=2
[VERSION]
Set_Version_Info=1
Company_Name=Framakey.org
File_Description=Compilateur pour lanceur de webapps
File_Version=2.0.0.0
Inc_File_Version=0
Internal_Name=CreationLanceur
Legal_Copyright=2011 Framakey.org
Product_Version=1.1.11.2
Set_AHK_Version=1
[ICONS]
Icon_1=%In_Dir%\CreationLanceur.ico

* * * Compile_AHK SETTINGS END * * *
*/

; ================================================================================
; 								CreationLanceur
; ================================================================================
;
; 	By fat115
; 	License : GNU/GPL v2
; 	This app is a specific compiler for Framakey's portable webapps launchers
;	Original AutoHotkeySC.bin is patched (VersionInfo and icons) before
;	adding portable launcher' script as RCDATA.
; 	It is not intended to replace finc's Ahk2Exe compiler which was a great resource to build this app.

; 	Compiler: AutoHotkey_L (http://www.autohotkey.com/).
; 	$id=FKPortableCompile.ahk $date=2013-08-23
;
; --------------------------------------------------------------------------------
;						Copyright (C) 2010-2013 Framakey

;						Website: http://www.framakey.org

/*
Changelog :
2013-08-23 : v 2.0.0.0
	reprise du FKPortableCompiler, tout est fait en interne, sauf la compression de l'exe final par upx
	basée sur AutohotKey_L 1.11.02
	
2010-02-27 : v 1.0.0.0
	Version initiale avec ahk2exe, AutoHotKeySC.bin, le tout en version 1.0.48.05
*/
#NoEnv
; On force le répertoire des includes à être celui du script
#Include %A_ScriptDir%
#Include Functions.ahk
#Include ini.ahk
#Include VersionInfo.ahk
#Include ResCompileU.ahk
#Include ScriptCompileU.ahk
#Include ChangeIcon.ahk
SetWorkingDir %A_ScriptDir%
OnExit, QuitCompiler
FileEncoding, UTF-8
SetTitleMatchMode 2
DetectHiddenWindows On

; création d'un dossier temporaire et extraction des fichiers contenus
Process, Exist
i_PID := ErrorLevel
p_PluginsDir := A_Temp "\FKPortableCompiler." i_PID
FileCreateDir, %p_PluginsDir%
FileInstall, upx.exe, %p_PluginsDir%\upx.exe
FileInstall, AutoHotkeySC.bin, %p_PluginsDir%\AutoHotkeySC.bin

; Définitions des éléments pour la récupération des IconGroups
CallB := RegisterCallback( "EnumResNameProc" )
IconGroups := ""

f_ScriptAhk := A_ScriptDir "\Source\WebAppPortable.ahk"

IfNotExist, %f_ScriptAhk%
	Msg_Error("Le script de la webapp (\Source\WebAppPortable.ahk) est impossible à trouver.`nL'auriez-vous renommé ?")

f_UpxExe := p_PluginsDir "\upx.exe"
f_AhkBin := p_PluginsDir "\AutoHotkeySC.bin"
f_RES_File := p_PluginsDir "\RCFile.res"

_spos := StringGetPos(f_ScriptAhk, "\", "R3")
p_BaseScriptDir := StringLeft(f_ScriptAhk, _spos)
f_AppInfo := p_BaseScriptDir "\App\AppInfo\appinfo.ini"
f_AppIcon := p_BaseScriptDir "\App\AppInfo\appicon.ico"

ini_load(s_AppInfoIniFile, f_AppInfo, "UTF-8")
s_PortableAppName := ini_getValue(s_AppInfoIniFile, "Details", "AppID")
FileRead, s_ScriptAhk, %f_ScriptAhk%
s_AHKVersion := FileGetVersion(f_AhkBin)

; on quitte proprement une éventuelle webapp déjà lancée (le WM risque de ne pas aimer)
if WinExist(s_PortableAppName ".exe ahk_class AutoHotkey")
	SendMessage, 0x6000, 2

o_Tableau := Object()
o_Tableau.CommaFileVersion := StringReplace(ini_getValue(s_AppInfoIniFile, "Version", "PackageVersion"), ".", ",", "All")
o_Tableau.CommaProductVersion := StringReplace(s_AHKVersion, ".", ",", 1)

o_Tableau.Comments := "Pour plus d'informations, visitez le site http://www.framakey.org"
o_Tableau.LegalCopyright := "© 2005-" A_Year " Framakey.org"
o_Tableau.CompanyName := "Framakey.org"
o_Tableau.FileDescription := s_PortableAppName " - " ini_getValue(s_AppInfoIniFile, "Details", "Description")
o_Tableau.FileVersion := ini_getValue(s_AppInfoIniFile, "Version", "PackageVersion")
o_Tableau.ProductVersion := s_AHKVersion
o_Tableau.InternalName := s_PortableAppName " " ini_getValue(s_AppInfoIniFile, "Version", "DisplayVersion")
o_Tableau.LegalTrademarks := ""
o_Tableau.OriginalFilename := ini_getValue(s_AppInfoIniFile, "Control", "Start")
o_Tableau.ProductName := s_PortableAppName
o_Tableau.BuildID := A_NowUTC

f_OutExe := p_BaseScriptDir "\" s_PortableAppName ".exe"
f_TempExe := p_PluginsDir "\TempExe.exe"

; création du script à compiler
f_ScriptAhk2Compile := f_ScriptAhk ".comp"
FileDelete, %f_ScriptAhk2Compile%
FileAppend, %s_ScriptAhk%, %f_ScriptAhk2Compile%

; on crée la chaine hexadecimale
s_VSVersion := CreateRESString(o_Tableau)

; on convertit en binaire
_nbBytes := StrLen(s_VSVersion) // 2
VarSetCapacity(_memdata, _nbBytes)
Loop, % _nbBytes
	_memadd := NumPut("0x" SubStr(s_VSVersion, (A_Index * 2) -1, 2), _memdata, A_Index-1, "UChar")

; on récupère les noms des IconGroups présents dans le binaire
_hModule := DllCall( "LoadLibraryEx", Str,f_AhkBin, UInt,0, UInt,0x2 )
DllCall("EnumResourceNames", UInt,_hModule, UInt,14, UInt,CallB, UInt,0 )
DllCall("FreeLibrary", UInt,_hModule )

; on patche le binaire
	_hModule := DllCall("BeginUpdateResource", Str, f_AhkBin, UInt, 0, Ptr)
	; mise en place de VERINFO
	DllCall("UpdateResource", Ptr, _hModule, Ptr, 16, Ptr, 1, UInt, 1033, Ptr, &_memdata, UInt, _nbBytes, UInt)
	; on boucle sur tous les groupes d'icones et on efface tout sauf le premier
	Loop, parse, IconGroups, |
	{
		If A_Index = 1
			{
			i_FirstIconID := A_LoopField
			Continue
			}
		DeleteIcons(_hmodule, A_LoopField, 1033, f_AhkBin)
	}
	; on met en place le premier groupe d'icone
	ReplaceIcons(_hmodule, f_AppIcon, i_FirstIconID, 1033, f_AhkBin)
DllCall("EndUpdateResource", Ptr, _hModule, UInt, 0)
VarSetCapacity(_memdata, 0)

; on compile
_compilsuccess := BundleAhkScript(f_ScriptAhk2Compile, f_TempExe, f_AhkBin)
If !_compilsuccess
	Msg_Error("La compilation a échoué.")

FileDelete, %f_ScriptAhk2Compile%
FileDelete, %f_OutExe%
RunWait, %f_UpxExe% -o"%f_OutExe%" "%f_TempExe%", , Hide
MsgBox, 64, Framakey Compiler, La création du lanceur est terminée., 3

Exit

;===============================================================================
; 							Subroutines
;===============================================================================

QuitCompiler:
	; On efface le dossier temporaire
	FileRemoveDir, %p_PluginsDir%, 1
ExitApp

;===============================================================================
; 							Functions
;===============================================================================

EnumResNameProc( hModule, lpszType, lpszName, lParam )
{ 
	Global IconGroups
	IconGroups .= ( ( IconGroups!="" ) ? "|" : "" ) lpszName 
	Return True 
}

Msg_Error(_txt, _options=16)
{
	Global b_ConsoleMode
	If b_ConsoleMode
		FileAppend, %_txt%, *
	Else
		MsgBox, %_options%, Framakey Compiler, %_txt%
	ExitApp, 1
}
