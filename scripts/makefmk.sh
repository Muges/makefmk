#!/usr/bin/bash

# get_ini fichier section option
# Renvoie la valeur d'une option dans un fichier .ini
function get_ini {
    if [ -z "${1:-}" ] || [ -z "${2:-}" ] || [ -z "${3:-}" ]; then
        message "Erreur : get_ini nécessite 3 arguments"
        message "get_ini fichier section option"
    fi
    awk -F'=' '/^\s*\['$2'\]\s*$/ { f = 1; next }
               /^\s*\[.*\]\s*$/ { f = 0; next }
               f == 1 && $1 ~ /\s*'$3'\s*/ { print $2 }' "$1" | tr -d '[[:space:]]'
}

# set_ini fichier section option valeur
# Modifie la valeur d'une option dans un fichier .ini
function set_ini {
    if [ -z "${1:-}" ] || [ -z "${2:-}" ] || [ -z "${3:-}" ]; then
        message "Erreur : set_ini nécessite 4 arguments"
        message "set_ini fichier section option valeur"
    fi
    awk -F'=' -v key="$3" -v val="$4" \
        '/^\s*\['$2'\]\s*$/ { f = 1; print $0; next }
         /^\s*\[.*\]\s*$/ { f = 0; print $0; next }
         f == 1 && $1 ~ /\s*'$3'\s*/ { print key "=" val; next }
         { print $0 }' "$1" > "$1.tmp"
    mv "$1.tmp" "$1"
}

if [ -z ${MAKEFMK_INSANDBOX:-} ] || [ $MAKEFMK_INSANDBOX != "true" ]; then
    message "Erreur : Le script makefmk.sh a été lancé en dehors de la sandbox."
    exit 1
fi

export sourcedir=$HOME/sources
export builddir=$HOME/build
export pkgdir=$HOME/$pkgname
export scriptdir=$HOME/scripts

# Vérifie que les fichiers nécessaires à la construction du paquet sont présents
# TODO: Vérifier que tous les fichiers sont présents (icones, ...) ?
if [ ! -f $pkgdir/App/AppInfo/appinfo.ini ]; then
    message "Erreur : le fichier $pkgdir/App/AppInfo/appinfo.ini n'existe pas."
    exit 1
fi
if [ ! -f $pkgdir/Other/Source/fmkbuild.sh ]; then
    message "Erreur : le fichier $pkgdir/Other/Source/fmkbuild.sh n'existe pas."
    exit 1
fi

message "-> Chargement du fichier fmkbuild.sh"

# Charge le fichier fmkbuild.sh
source $pkgdir/Other/Source/fmkbuild.sh

# Vérifie que le fichier fmkbuild.sh est complet
if [ "$(type -t build)" != "function" ]; then
    message "Erreur : le fichier fmkbuild.sh ne contient pas de fonction build"
    exit 1
fi
if [ "$(type -t package)" != "function" ]; then
    message "Erreur : le fichier fmkbuild.sh ne contient pas de fonction package"
    exit 1
fi
if [ -z ${pkgver:-} ]; then
    message "Erreur : le fichier fmkbuild.sh ne contient pas de numéro de version (pkgver)"
    exit 1
fi
if [ -z ${pkgrel:-} ]; then
    message "Erreur : le fichier fmkbuild.sh ne contient pas de numéro de release (pkgrel)"
    exit 1
fi

message "-> Téléchargement des sources"

for source in "${sources[@]}"
do
    if [[ $source =~ (.*)::(.*) ]]; then
        # La source est sous la forme "$filename::$url"
        filename=${BASH_REMATCH[1]}
        source=${BASH_REMATCH[2]}
    else
        # La source est sous la forme "$url"
        filename=$(basename $source)
    fi
    if [ ! -f $sourcedir/$filename ]; then
        wget $source -O $sourcedir/$filename
    fi
    cp $sourcedir/$filename $builddir
done

message "-> Installation de l'application"
build

message "-> Construction du paquet"
package

if [ -z ${upx:-} ] || [ $upx != "true" ]; then
    message "-> Compression des fichiers dll et exe"

    find $pkgdir -iname "*.exe" -o -iname "*.dll" | xargs upx -qqq || true
fi

message "-> Compilation du lanceur"

ahkfiles=($pkgdir/Other/Source/*Portable.ahk)
for ahkfile in "${ahkfiles[@]}"
do
    # TODO : réécrire fkportablecompiler en bash
    wine `winepath -w "$scriptdir/FKPortableCompiler.exe"` `winepath -w $ahkfile`
done

message "-> Mise à jour du fichier appinfo.ini"

set_ini "$pkgdir/App/AppInfo/appinfo.ini" Version PackageVersion "$pkgver"

lang=$(get_ini "$pkgdir/App/AppInfo/appinfo.ini" Details Language)
if [ $lang == "Français" ]; then
    lang="fr"
elif [ $lang == "English" ]; then
    lang="en"
elif [ $lang == "Multilingue" ]; then
    lang="mult"
fi

dispver=$(echo $pkgver | sed 's/\(\.0\)*$//')
set_ini "$pkgdir/App/AppInfo/appinfo.ini" Version DisplayVersion "$dispver $lang Release $pkgrel"

date=$(date +"%Y/%m/%d")
set_ini "$pkgdir/App/AppInfo/appinfo.ini" Framakey Date "$date"

size=$(du -sb $pkgdir | sed -r 's/^([0-9]+).*$/\1/')
set_ini "$pkgdir/App/AppInfo/appinfo.ini" Framakey Size "$size"

# Crée le paquet
message "-> Compression du paquet"

cd $pkgdir/..

pkgfile="${pkgname}_$dispver-$lang-r$pkgrel.fmk.zip"
zip -q -r $pkgfile $pkgname

message "-> Le paquet $pkgfile a été créé"
